#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define MAX_TEXT 100
#define MAX_CAR 50
#define MAX_REPETICIONS 50
#define MAX_CODIGS 150
#define MAX_DIGITS 50
int obtindrePosicioC(char text[],int *i,char caracters[]);
bool esbuit(char caracters[],int *ic);
void copiarAfegir(char text [],int *i, int *ic, char caracters[],int repeticions[]);
int longitud(char caracters[]);
void ordenar(int repeticions[],int t,char caracters[]);
void descompactacio(bool trobat,char text3[],int *it2,int ic,char caracters[]);
void cercarCodi(char codigs[MAX_DIGITS][MAX_CODIGS],char codi[],int *it2,char text3[],char caracters[]);
void saltarDolar(char text2[],int *itc);
void obtenirCodi(char text2[],int *itc,char codi[]);
void descompactar(char text2[],char text3[],char codigs[MAX_DIGITS][MAX_CODIGS],char caracters[]);
bool iguals(char par1[], char par2[]);

int main(){
	char text[MAX_TEXT+1];
	char text2[MAX_TEXT+1];
	char text3[MAX_TEXT+1];
	char caracters[MAX_CAR+1];
	char codigs[MAX_DIGITS][MAX_CODIGS]={"0\0","1\0","00\0","01\0","10\0","11\0","000\0","001\0","010\0","011\0","100\0","110\0","111\0","0000\0","0001\0","0010\0","0101\0","0110\0","0111\0","1000\0","1001\0","1010\0","1011\0","1100\0","1101\0","1111\0","00000\0","\0"};
	int repeticions[MAX_REPETICIONS];
	int i,ic,t,ip,in;

	text[0]='\0';
	for(int i=0;i<MAX_CAR;i++){
    	caracters[i]='\0';
	}
	for(int i=0;i<MAX_REPETICIONS;i++){
    	repeticions[i]=0;
	}

	printf("Introduir text: \n");
	scanf("%100[^\n]",text);

	i=0;
	while(text[i]!='\0'){
    	//obtindre posicio caracter
    	ic = obtindrePosicioC(text,&i,caracters);
    	//////////////////////////
    	//es fina copia si no conta una repeticio
    	copiarAfegir(text,&i,&ic,caracters,repeticions);
    	i++;
	}
	//longitud
	t=longitud(caracters);
	ordenar(repeticions,t,caracters);
	//////////////////////////////////////////////////////

	////////////  imprimir   //////////////////////
	i=0;
	while(i<t){
    	printf("El caracter %c te %d repeticions\n",caracters[i],repeticions[i]);
    	i++;
	}
	printf("Llista car \t Llista repe \t Codigs\n");
	i=0;
	while(i<t){
    	printf("%c \t\t %d \t\t %s \n",caracters[i],repeticions[i],codigs[i]);
    	i++;
	}
	//////////////////////////////////////////////////////////////
	codificar(text,caracters,codigs,text2);
	i=0;

	while(text[i]!='\0'){
        	i++;
	}
	t=0;
	while(text2[t]!='\0'){
        	t++;
	}

	printf("Bits del text compactat: %d\n",t-i);
	printf("%% de compatacio: %d \n",100-(((t-i)*100)/(i*8)));
	printf("text compactat: %s\n",text2);
  //  printf("text descompactat: %s",text);

 /*   printf("Introduir text compactat:\n");
	scanf("%100[^n]",text3);
*/

    	//descodificar
   	/* it=0;
    	itc=0;
    	while(codigs[it][itc]!='\0'){
        	while(text2[itc]!='\0'&&text2[itc]!='$'){
        	itc++;
        	}
        	ip=0;
        	while(codigs[it][ip]==text2[itc]||text2[itc]!='\0' &&codigs[it][ip]!='\0'){
            	itc++;ip++;
        	}
        	if(codigs[it][ip]==text2[itc]){
            	text3[itc]=caracters[it];
        	}
        	it++;

    	}
    	*/
	descompactar(text2,text3,codigs,caracters);
	printf("\nEl text descompactat es: %s ", text3);

	return 0;
}
/*
NOM:  codificar
DESCRIPCI�: codifica
VARIABLES: char text[],char caracters[],char codigs[][MAX_CODIGS],char text2[]
PRECONDICIO: El text ha de acabar en \0.
Exemple:    text[]={�h�,�o�,�l�,�a�,};
        	caracters[]={�h�,�o�,�l�,�a�,};
        	codigs[]={�0�,�1�,�01�,�10�};
        	///////////////////////////
            text2[]={�0�,�1�,�01�,�10�};
*/
codificar(char text[],char caracters[],char codigs[][MAX_CODIGS],char text2[]){
	int ip=0;
	int i=0;
	int in=0;
	while(text[i]!='\0'){
        in=obtenerPosicion(caracters,&i,text);
        copiarCodigo(codigs,&in,&i,&ip,text2);
        }
    text2[ip-1]='\0';
}
/*
NOM:  copiarCodigo
DESCRIPCI�: des de la posicion obtenida del vector del caracteres i copia el codigo de esa posicion al texto2
VARIABLES: char text2[], char codigs[][], int *in,int *i,int *ip
PRECONDICIO: El vector de caracters inicialitzat amb \0 per poder fer la busqueda per centinella.
EXAMPLE:	caracters[]={�h�,�o�,�l�,�a�,};
            codigs[]={�0�,�1�,�01�,�10�};
            caracters[i]="h";
            codigs[i]="0";
        	//////////////
        	text[]="h"


*/
void copiarCodigo(char codigs[][MAX_CODIGS],int *in,int *i,int *ip,char text2[]){
    int ic=0;
	while(codigs[*in][ic]!='\0'){
    	text2[*ip]=codigs[*in][ic];
    	ic++;(*ip)++;
	}
	text2[*ip]='$';
    (*ip)++;

    (*i)++;
}
/*
NOM:  obtenerPosicion
DESCRIPCI�: Obte la posicio del caracter dintre del vector de caracters
VARIABLES: char text[], char caracters[], int *i
PRECONDICIO: El vector de caracters inicialitzat amb \0 per poder fer la busqueda per centinella.
EXAMPLE:	text[]={�h�,�o�,�l�,�a�,};
        	caracters[]={�h�,�o�,�l�,�a�,};
        	*i=2
        	//////////////
        	resultat=o


*/
int obtenerPosicion (char caracters[],int *i,char text[]){
 int in=0;
	//obtener posicion
	while(caracters[in]!=text[*i]){
    	in++;
	}
	return in;
}
/*
NOM:  obtindrePosicioC
DESCRIPCI�: Obte la posicio del caracter dintre del vector de caracters
VARIABLES: char text[], char caracters[], int *i
PRECONDICIO: El vector de caracters inicialitzat amb \0 per poder fer la busqueda per centinella.
EXAMPLE:	text[]={�h�,�o�,�l�,�a�,};
        	caracters[]={�h�,�o�,�l�,�a�,};
        	*i=2
        	//////////////
        	resultat=o


*/
int obtindrePosicioC(char text[],int *i,char caracters[]){
	int ic=0;
	while(caracters[ic]!=text[*i] && caracters[ic]!='\0')ic++;
	return ic;
}
/*
NOM:  esbuit
DESCRIPCI�: Mira si en la posicio *ic del vector de caracters esta buit
RETORN:  true o false
VARIABLES:  char caracters[], int *ic
PRECONDICIO: El vector de caracters inicialitzat.
EXAMPLE:	caracters[]={�h�,�o�};
        	ic=2
        	/////
        	resultat false
*/

bool esbuit(char caracters[],int *ic){
	return caracters[*ic]=='\0';
}
/*
NOM:  copiarAfegir
DESCRIPCI�: copiar els caracters del text al vector de caracters i afegeix una repeticio al vector de repeticions si es repeteix el caracter mes d'una vegada
RETORN:  	res
VARIABLES: char text[], char caracters[],int repeticions[],int *i,int *ic
PRECONDICIO: El vector de caracters inicialitzat amb \0 per poder fer la busqueda per centinella.
EXAMPLE: 	text[]={�h�,�o�,�l�,�a�,};
        	caracters[]={�h�,�o�,�l�,�a�,};
        	repeticions[]={0,0,0,0};

*/
void copiarAfegir(char text[],int *i, int *ic, char caracters[],int repeticions[]){
  if(esbuit(caracters,ic)){
    	caracters[*ic]=text[*i];
	}else{
    	repeticions[*ic]++;
	}
}
/*
NOM:  longitud
DESCRIPCI�:calcula la longitud .
RETORN:  	res
VARIABLES:  char caracters[]
PRECONDICIO: Que hi hagi algo al vector o text que volem calcular la seva longitud
EXAMPLE:
caracters[]={�h�,�o�,�a�,};
/////////////////////////////////////////
3

*/
int longitud(char caracters[]){
	int i=0;
	while(caracters[i]!='\0')i++;
	return i;
}
/*
NOM:  ordenar
DESCRIPCI�: ordena el vector d�enters del m�s gran al m�s petit i el vector de caracters tenint en compte les repeticions.
RETORN:  	res
VARIABLES: int repeticions[], char caracters[]
PRECONDICIO: Hem de saber quin es el maxim de repeticions o de caracters hi ha dintre del vector.
EXAMPLE:
caracters[]={�h�,�o�,�a�,};
repeticions[]={0,1,0};
/////////////////////////////////////////
caracters[]={�o�,�h�,�a�,};
repeticions[]={1,0,0};

*/
void ordenar(int repeticions[],int t,char caracters[]){
	char auxcar;
	int aux=0;
    for(int i=2;i<=t;i++){
        for(int c=0;c<=t-i;c++){
            if(repeticions[c]<repeticions[c+1]){
                aux=repeticions[c];
                repeticions[c]=repeticions[c+1];
                repeticions[c+1]=aux;

                auxcar=caracters[c];
                caracters[c]=caracters[c+1];
                caracters[c+1]=auxcar;
                }
            }
        }

}
/*
NOM:  descompactar
DESCRIPCI�: descompacta el text2
RETORN:  	res
VARIABLES: char text[], char caracters[],int repeticions[] , codigs [][]
PRECONDICIO: El vector de caracters inicialitzat amb \0 per poder fer la busqueda per centinella.
EXAMPLE:	text[]={�h�,�o�,�l�,�a�,};
        	caracters[]={�h�,�o�,�l�,�a�,};
        	codigs[]={�0�,�1�,�01�,�10�};
        	text2[]={�0�,�1�,�01�,�10�};
        	///////////////////////////
        	text3[]={�h�,�o�,�l�,�a�,};
*/
void descompactar(char text2[],char text3[],char codigs[MAX_DIGITS][MAX_CODIGS],char caracters[]){
	int itc=0;
	int it2=0;
	char codi[MAX_CODIGS];
	while(text2[itc]!='\0'){
    	//obtenir codi
    	obtenirCodi(text2,&itc,codi);
    	//saltar marca final de codi, el '$'
    	saltarDolar(text2,&itc);
    	//cercar el codi "buscacodis" a la llista de paraules "codis"
    	cercarCodi(codigs,codi,&it2,text3,caracters);
	}
	text3[it2]='\0';
}
/*
NOM:  obtenir codi
DESCRIPCI�: obte el codi que hi ha entre els dolars o el inici i un dolar o un dolar i el final que hi ha en el text2
RETORN:  	res
VARIABLES: char text[], int *itc, codi []
PRECONDICIO: El tect ha de acabar amb \0.
EXAMPLE:    text2[]={�0�,"$",�1�,"$",�01�,"$",�10�};
        	///////////////////////////
        	codi[]{"1"}
*/
void obtenirCodi(char text2[],int *itc,char codi[]){
	int ic=0;
	while(text2[*itc]!='$' && text2[*itc]!='\0'){
    	codi[ic]=text2[*itc];
    	ic++;(*itc)++;
	}
	codi[ic]='\0';
}
/*
NOM:  saltartDolar
DESCRIPCI�: salta el dolar que hi ha entre codigs(paraules)
RETORN:  	res
VARIABLES: char text2[], int *itc,
EXAMPLE:    text2[]={�0�,"$",�1�,"$",�01�,"$",�10�};
            *itc=2
            text2[*itc]={"$"}
        	///////////////////////////
        	En aquet cas saltaria el dolar
*/
void saltarDolar(char text2[],int *itc){
    	if(text2[*itc]=='$') (*itc)++;
}
/*
NOM:  cercar codi
DESCRIPCI�: busque el codi que pertany dintre del vector de codigs
RETORN:  	res
VARIABLES: char codigs [][],char codi[],int *it2,char text3[], char caracters[]
PRECONDICIO: El vector de codigs inicialitzat amb \0 per poder fer la busqueda per centinella.
EXAMPLE:	codi[]={�0�,�1�,�0�,�1�,};
        	codigs[][]={�0�,�1�,�01�,�10�};
        	codi[1]={"1"};
        	codigs[1]={"0"}
        	///////////////////////////
        	en aquet cas anirie a buscar el seg�ent codi de la llista de codigs
*/
void cercarCodi(char codigs[][MAX_CODIGS],char codi[],int *it2,char text3[],char caracters[]){
	int ic=0;
	bool trobat=false;
	while(codigs[ic][0]!='\0'){
    	if(iguals(codi,codigs[ic])){trobat=true;break;
    	}else ic++;
	}
	descompactacio(trobat,text3,it2,ic,caracters);
}
/*
NOM:  igual
DESCRIPCI�: mire si dues paraules son iguals
RETORN:  	true or false
VARIABLES: char par1[],char par2[]
PRECONDICIO: Les dues paraules han d'estar inicialitzades
EXAMPLE:	par1[]={"q","u","e"};
            par2[]={"q","u","e"};
            ////////////////////
            return true;
*/
bool iguals(char par1[], char par2[]){
	bool iguals=false;
	int i=0;
	while(par1[i]==par2[i] && (par1[i]!='\0' || par2[i]!='\0')) i++;
	if(par1[i]==par2[i]) iguals=true;
	return iguals;
}
/*
NOM:  descompactacio
DESCRIPCI�: si el troba el caracter copia a text3
RETORN:  	res
VARIABLES: bool trobat,char text3[],int *it2,int ic ,char caracters[]
EXAMPLE:	tobat = true
            text3[*it2]       = caracters[ic]
            (*it2=2)            (ic=2)
            (text3[2]"h","*")   (caracters[2]="h")

*/
void descompactacio(bool trobat,char text3[],int *it2,int ic,char caracters[]){
	if(trobat){
    	text3[*it2]=caracters[ic];
    	(*it2)++;
	}
}



